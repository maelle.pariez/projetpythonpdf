#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------
""" PDF Storage & Extraction """

# Dépendances
try:
    import json
    from controler import data_from_pdf, extract_information
    from sqlalchemy import Column, Integer, String, create_engine, select
    from sqlalchemy.ext.declarative import declarative_base
    from sqlalchemy.orm import sessionmaker

    print("model.py : Tous les modules sont bien téléchargés.")
except ModuleNotFoundError():
    print("model.py : Modules manquants. Veuillez les installer.")


# Creation ou connexion à la BDD
engine = create_engine("sqlite:///databasepdf.db", echo=True)
Base = declarative_base(bind=engine)


def create_session():
    """Fonction de création de session"""
    ma_session = sessionmaker(bind=engine)
    session = ma_session()
    return session


class Data(Base):
    """Creation de la base de données (BDD)"""

    __tablename__ = "datafrompdf"
    id = Column(Integer, primary_key=True)
    status = Column(String, default="Pending")
    author = Column(String)
    creator = Column(String)
    producer = Column(String)
    subject = Column(String)
    title = Column(String)
    content = Column(String)

    def __init__(self, status, author, creator,
                 producer, subject, title, content):
        """Initialisation"""
        self.status = status
        self.author = author
        self.creator = creator
        self.producer = producer
        self.subject = subject
        self.title = title
        self.content = content

    def affiche_auteur(self):
        """Affiche l'auteur"""
        print(self.author)

    def affiche_titre(self):
        """Affiche le titre"""
        print(self.titre)


# Create or open BDD
Base.metadata.create_all(engine)


def create_bdd(path):
    """Création de la base de données (BDD) et stockage du contenu.
    Statut "Success" si le document a un titre, un auteur et du texte.
    Statut "Failure" si pas de titre ou pas d'auteur ou pas de texte.
    Return :
        L'ID du PDF téléchargé dans la BDD.
    """
    session = create_session()
    # Adding objects in database
    information = extract_information(path)
    auteur = str(information.author)
    createur = str(information.creator)
    producteur = str(information.producer)
    sujet = str(information.subject)
    titre = str(information.title)
    texte = data_from_pdf(path)
    if (texte == "None") or (sujet == "None") or (auteur == "None"):
        status = "Failure"
    else:
        status = "Success"
    datafrompdf = Data(
        author=auteur,
        status=status,
        creator=createur,
        producer=producteur,
        subject=sujet,
        title=titre,
        content=texte,
    )
    session.add(datafrompdf)
    session.commit()
    # show_content_table(datafrompdf)
    idreturn = datafrompdf.id
    session.close()
    return idreturn


def extract_data(doc_id):
    """Extraction du contenu de la base de données selon l'ID choisi
    Stockage du contenu dans un objet
    Return :
        Objet Json contenant les métadonnées et le texte
    """
    json_data = {}
    requete = select(Data).where(Data.id == doc_id)
    session = create_session()
    result = session.execute(requete)
    data = {}
    for datafrompdf in result.scalars():
        data["status"] = datafrompdf.status
        data["author"] = datafrompdf.author
        data["creator"] = datafrompdf.creator
        data["producer"] = datafrompdf.producer
        data["subject"] = datafrompdf.subject
        data["title"] = datafrompdf.title
        data["content"] = datafrompdf.content
    if not data:
        data["status"] = ""
        data["title"] = ""
        data["content"] = ""
    # Conversion en Json String
    json_data = json.dumps(data)
    session.close()
    # return Response(json_data, mimetype="application/json;charset=utf-8")
    return json_data


def create_text_file(doc_id):
    """Extraction du contenu texte depuis la base de données selon l'ID choisi
    Stockage du contenu dans un fichier .txt
    Return :
        Objet Json contenant les métadonnées et le texte
    """
    info = extract_data(doc_id)
    obj_python = json.loads(info)
    content = obj_python["content"]
    # Convertion en Json String
    json_content = json.dumps(content)
    # Stocker le contenu du pdf dans un fichier texte ID.txt
    with open(
        "uploadfiles/" + str(doc_id) + ".txt", "w", encoding="utf-8"
    ) as fichier:
        fichier.write(json_content)
    return info


def show_content_table(datafrompdf):
    """Affiche le contenu de la table pour vérifier les données
    Return :
        L'ID du document
    """
    print("----------------------------------")
    print("Affichage du contenu de la table")
    # query = session.query(Data)
    # instance = query.first()
    print(datafrompdf.id)
    print(datafrompdf.title)
    print(datafrompdf.author)
    print(datafrompdf.creator)
    print(datafrompdf.producer)
    print(datafrompdf.subject)
    print(datafrompdf.content)
    print("---------------------------------")
    return datafrompdf.id
