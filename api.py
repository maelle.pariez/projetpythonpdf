# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------

""" PDF Storage & Extraction """

from flask import Flask


# def create_app(test_config=None):
def create_app():

    """Create and configure the flask app with the factory pattern"""
    return Flask(__name__, instance_relative_config=True)
