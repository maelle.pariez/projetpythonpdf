FROM python:3.9

# Copie des fichiers dans le conteneur
COPY . /projetpythonpdf/

# Définit le répertoire de travail dans le conteneur
WORKDIR /projetpythonpdf/

# Installe les dépendances de l'application à partir du fichier requirements.txt
RUN pip install -r requirements.txt

# Expose le port 5000 pour les connexions entrantes
EXPOSE 5000

# Démarre l'application en utilisant le fichier main.py
CMD ["python3", "main.py", "--host=0.0.0.0", "--port=5000"]