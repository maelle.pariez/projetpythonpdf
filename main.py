# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------
""" PDF Storage & Extraction """

from view import app

# main
if __name__ == "__main__":
    # run app in debug mode on port 5000
    print("Serveur lancé !")
    app.run(host='0.0.0.0', port=5000)

    # Mode Debug puissant
    # app.run(debug=True, use_debugger=False, use_reloader=False)
