# ProjetPythonPDF

ProjetPythonPDF est un outil (web service) développé en python permettant d'extraire le contenu d'un fichier PDF.
Cette API RESTful permet à un utilisateur de verser un PDF (avec une requête POST HTTP) afin de pouvoir :
 - Extraire les métadonnées et le contenu texte du PDF. 
 - Stocker ces informations dans une base de donnée.
 - Enregistrer le contenu texte du PDF dans un fichier texte (.txt).
 - Récupérer les informations (texte et métadonnées) via une requête HTTP GET.
 - D'obtenir le statut du document pour savoir s'il est exploitable en l'état en vue d'un traitement ultérieur des métadonnées (fonction utile pour le projet fil rouge).

Le code a été formaté avec Black & Isort et vérifié avec Flake8. Il a également été testé avec pylint, pytest.


# Installation du module "PythonProjetPDF" 

## Avec Linux ou Debian

### Avec git (si vous n'avez pas le projet en local)

#### Prérequis : installer git

    sudo apt install git

#### Récupérer le projet dans un dossier localement :
    
    mkdir ProjetPythonPDF
    cd ProjetPythonPDF
    git clone https://gitlab-student.centralesupelec.fr/maelle.pariez/projetpythonpdf

    cd projetpythonpdf

### Sans Git (vous avez déjà le projet en local)

    cd projetpythonpdf

## Dependances 

Installer les dépendances de ProjetPythonPDF :

    pip install -r requirements.txt


## Avec MacOS ou Windows

    Veuillez utiliser docker.

## Avec Docker

    (sudo) docker build -t extractpdf .

# Lancement du projet sur docker

    (sudo) docker run -d -p 5000:5000 --name extractpdf extractpdf


# Utilisation de ProjetPythonPDF sans docker

Pour utiliser l'application, commencez par lancer le serveur en exécutant la commande suivante dans votre terminal :

    python3 main.py


## Utilisation par ligne de commande

Verser un pdf de test :
    
    curl -X POST -F "file=@tests/MesPDFdeTest/22.pdf" http://localhost:5000

Tester en affichant les métadonnées :

    curl localhost:5000/metadata/1

Tester le téléchargement du texte contenu dans le pdf (affiche le contenu texte et créee un fichier .txt):

    curl localhost:5000/downloads/1

Tester l'affichage du statut du document (le statut sera "failure" car le document n'a pas de titre):
Si vous testez avec le document "2210.14739.pdf" qui a un titre, vous obtiendrez le statut "success"

    curl localhost:5000/status/1

Tester l'affichage du titre du document :

    curl localhost:5000/title/1

Tester l'affichage du texte du document sans l'enregistrer dans un fichier .txt :

    curl localhost:5000/text/1


## Utiliser l'API avec l'interface graphique

Une fois le serveur lancé, ouvrez l'URL http://127.0.0.1:5000/ dans un navigateur de votre choix. Vous pourrez alors utiliser l'application en envoyant des requêtes HTTP à l'aide de l'interface RESTful fournie.

## Versement d'un PDF

Pour envoyer un PDF à l'application, cliquez sur le bouton "Choisir un fichier" et sélectionnez le fichier souhaité. Par exemple "\tests\MesPDFdeTest\22.pdf", cliquez ensuite sur le bouton "Télécharger".

L'application vous attribuera alors un identifiant unique pour ce document, que vous pourrez utiliser pour accéder aux métadonnées et au contenu du document via des requêtes HTTP.

Réponse :

    - Si un fichier a correctement été téléchargé :
        Message flash : Votre fichier <NomDuFichier>.pdf a bien été téléchargé !
        Son identifiant est le : <identifiant>
        Si c'est le premier, l'identifiant sera 1

Gestion des erreurs :

    - Si aucun fichier n'est téléchargé : 
        Message flash : Aucun fichier n'a été téléchargé. Veuillez recommencer.

    - Si le format de fichier n'est pas un .pdf :
        Message flash : Format de fichier invalide. Choisir un fichier .pdf


## Récapitulatif des fonctionnalités possibles

L'utilisateur peut utiliser les endpoints suivants pour obtenir des informations.

Pour verser un PDF :
    http://127.0.0.1:5000/

Sans saisir d'identifiant, obtenir les informations du PDF tout juste versé :
    http://127.0.0.1:5000/metadata
    http://127.0.0.1:5000/title
    http://127.0.0.1:5000/titre
    http://127.0.0.1:5000/metadonnees
    http://127.0.0.1:5000/texte


Obtenir des informations à l'aide d'un identifiant :
    http://127.0.0.1:5000/text/identifiant
    http://127.0.0.1:5000/status/identifiant
    http://127.0.0.1:5000/downloads/identifiant
    http://127.0.0.1:5000/metadata/identifiant
    http://127.0.0.1:5000/title/identifiant

Gestion des erreurs :
    Redirection vers la page d'accueil si l'URL saisie n'existe pas.


## AFFICHAGE BRUT SANS STOCKAGE

### Afficher le contenu texte du fichier tout juste versé par l'utilisateur

Saisir l'URL :

    http://127.0.0.1:5000/texte

Réponse :

    Affiche le texte du fichier courant sans le stocker dans une base de données.

### Afficher les métadonnées du fichier tout juste versé par l'utilisateur 

Saisir l'URL : 

    http://127.0.0.1:5000/metadonnees
    
Réponse :

    Affiche les métadonnées du fichier courant sans les stocker dans une base de données.

### Afficher le titre du fichier tout juste versé par l'utilisateur

Saisir l'URL :

    http://127.0.0.1:5000/titre

Réponse :

    Affiche le titre du fichier courant sans le stocker dans une base de données.


## STOCKAGE EN BASE DE DONNEES ET AFFICHAGE

### SANS NOTION D'IDENTIFIANT

#### Afficher le titre du document versé

Saisir l'URL :

    http://127.0.0.1:5000/title

Réponse :

   Affiche le titre du fichier et de le stocker dans une base de données.

#### Afficher les métadonnées du document versé

Saisir l'URL :

    http://127.0.0.1:5000/metadata
    
Réponse :

    Affiche les métadonnées du fichier et de les stocker dans une base de données.

### RECHERCHE PAR IDENTIFIANT

#### Afficher le titre d'un fichier selon son identifiant

L'identifiant est un nombre entier. Saisir un identifiant dans l'URL :

    http://127.0.0.1:5000/title/<ID>

Réponse :

    Affiche le titre du document correspondant à l'ID saisi.

Exemple : 
    
    http://127.0.0.1:5000/title/1
    
    Affiche le titre du fichier corespondant à l'identifiant 1.

#### Afficher les métadonnées d'un document selon son identifiant

L'identifiant est un nombre entier. Saisir un identifiant dans l'URL :

    http://127.0.0.1:5000/metadata/<ID>

Réponse :

    Affiche les métadonnées du document correspondant à l'ID saisi.
    Les données sont stockés dans un objet Json et correspondent à une chaîne de caractère (string) :
    - status : valeur possible "Failure" ou "Pending" ou "Success"
    - author : correspond à la valeur "auteur" du document dans les métadonnées du pdf
    - creator : correspond à la valeur "créateur" du document dans les métadonnées du pdf
    - producer : correspond à la valeur "producteur" du document dans les métadonnées du pdf
    - subject : correspond à la valeur "suject" du document dans les métadonnées du pdf
    - title : correspond à la valeur "titre" du document dans les métadonnées du pdf
    - content : correspond à la valeur "contenu" du document dans les métadonnées du pdf

Exemple : 

    http://127.0.0.1:5000/metadata/1

    Affiche les métadonnées du fichier corespondant à l'identifiant 1.

#### Afficher le statut d'un document selon son identifiant

L'identifiant est un nombre entier. Saisir un identifiant dans l'URL : 

    http://127.0.0.1:5000/status/<ID>

Réponse :

    Si le PDF n'a pas de titre ou pas de sujet ou pas de contenu alors son statut est : failure
    Si le PDF a un titre, un sujet et du contenu alors son statut est : success
    Sinon, son statut est : pending

    Choix effectué pour simplifier la phase nettoyage des données dans le cadre du projet fil rouge.
    En effet, afin de pouvoir faire des rapprochements entre auteurs, les fichiers doivent avoir certaines caractéristiques.
    Les documents qui n'ont pas de titre, de contenu ou d'auteur seront écartés dans un premier temps.

Exemple : 
    
    http://127.0.0.1:5000/status/2
    
    Affiche le statut du document ayant l'identifiant 2.


#### Afficher le contenu texte d'un fichier selon son identifiant

Saisir un identifiant (nombre entier) dans l'URL : 
    
    http://127.0.0.1:5000/text/<ID>

Réponse : 
    
    Affiche le contenu texte du fichier correspondant à l'identifiant saisi.
    
Exemple : 

    http://127.0.0.1:5000/text/1

    Affiche le contenu texte du fichier ayant 1 comme identifiant dans la base de données.

#### Créer un fichier .txt correspondant au contenu texte d'un fichier selon son identifiant et l'afficher

Saisir un identifiant (nombre entier) dans l'URL : 

    http://127.0.0.1:5000/downloads/<ID>

Réponse :

    Affiche le contenu texte selon l'ID saisir par l'utilisateur.
    Le contenu texte est récupéré depuis la base de données.
    Le contenu est stocké dans un fichier ID.txt
    Le contenu de ce fichier est lu et affiché.   
   
Exemple : 
    
    http://127.0.0.1:5000/downloads/2

    Enregistre le fichier 2.txt dans le dossier /uploadfiles/ avec le contenu texte du fichier pdf enregistré avec l'identifiant 2.


# Formatage et vérification du code

Le code a été formaté et vérifié avec Black, Isort et Flake8 pour s'assurer de sa qualité et de sa conformité aux conventions de style

Pour formater le code avec Black et Isort, exécutez les commandes suivantes dans le répertoire du projet :
    python3 -m black ProjetPythonPDF
    python3 -m isort ProjetPythonPDF

Ou avec les commandes suivantes pour cibler un fichier :
    python3 -m black nomdufichier.py
    python3 -m isort nomdufichier.py


Pour vérifier la qualité du code avec Flake8, exécutez la commande suivante :
    python3 -m flake8

Certaines contradictions existent toutefois entre les règles de formatage (Black & Isort) et la vérification du code (flake8) notamment concernant la longueur des lignes. La propreté du code peut avoir un impact sur la qualité et la maintenabilité du code. Ainsi, les règles de flake8 ont été privilégié.


# Tester le code avec pytest

Le code a été testé et validé avec Pylint et Pytest :

    python3 -m pytest

Le résultat de la commande suivante (et donc des tests) se trouve dans un fichier "tests/resultats_tests.txt :

    python3 -m pytest > tests/resultats_tests.txt
