# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------
""" PDF Storage & Extraction """

# Dépendances
try:
    import json
    import os
    import uuid
    from api import create_app
    from controler import data_from_pdf, extract_information
    from flask import flash, redirect, render_template, request
    from model import create_bdd, create_text_file, extract_data
    from werkzeug.utils import secure_filename
    print("view.py : Tous les modules sont bien téléchargés.")
except ModuleNotFoundError():
    print("view.py : Modules manquants. Veuillez les installer.")

app = create_app()

# Configuration
UPLOAD_FOLDER = "uploadfiles"  # Dossier de destination des PDF téléchargé
HEX_SEC_KEY = uuid.uuid4().hex  # Définition d'une clef secrète hexadecimale
ALLOWED_EXTENSIONS = {"pdf"}  # Format de fichier autorisé : pdf
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER  # Configure le répertoire d'upload
app.config["SECRET_KEY"] = HEX_SEC_KEY  # Configuration de la clef secrète
ID = ""
PATH = ""


def allowed_file(filename):
    """Renvoie vrai si le fichier possède une extension valide
    L'extension valide est en .pdf
    """
    return (
        "." in filename and filename.rsplit(".", 1)[1].lower()
        in ALLOWED_EXTENSIONS
    )


@app.errorhandler(404)
def not_found(err):
    """Gestion des erreurs 404
    Page introuvable
    Return :
        Page d'accueil index.html
    """
    # print("Traitement de l'erreur : " + str(err))
    flash("Cette page n'existe pas. Lisez README.md")
    return render_template("index.html")


@app.route("/", methods=["GET", "POST"])
def upload_file():
    """URL de la page d'accueil
    Méthode GET permet d'afficher une page HTML avec un formulaire d'envoi
    Méthode POST permet à l'utilisateur de verser un PDF
    Affiche des messages flash pour guider l'utilisateur
    Return :
        Le fichier HTML
    """
    if request.method == "POST":
        # Vérifie si la requête post contient bien un fichier
        if "file" not in request.files:
            flash("Pas de fichier téléchargé.")
            ValueError("No file download.")
            return redirect(request.url)
        file = request.files["file"]
        # Vérification du nom du fichier
        if file.filename == "":
            ValueError("No name because no file.")
            flash("Aucun fichier n'a été téléchargé. Veuillez recommencer.")
            return redirect(request.url)
        if not allowed_file(file.filename):
            ValueError("Invalid file's format.")
            flash("Format de fichier invalide. Choisir un fichier .PDF")
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # Sauvegarde du fichier en local dans le répertoire d'upload
            file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            flash("Votre fichier " + filename + " a bien été téléchargé !")
            global PATH
            PATH = "uploadfiles/" + filename
            global ID
            ID = create_bdd(PATH)
            flash("")
            flash("Son identifiant est le : " + str(ID))
            return render_template("download_files.html")
    return render_template("index.html")


@app.route("/text/<int:doc_id>")
def show_content_text(doc_id):
    """Affiche le contenu texte d'un PDF depuis la base de données selon l'ID
    Return :
        Contenu texte du PDF selon l'id saisi
        Si l'ID saisi par l'utilisateur n'existe pas en BDD : exception
    """
    try:
        info = extract_data(doc_id)
        obj_python = json.loads(info)
        content = obj_python["content"]
        if obj_python["content"] == "":
            return "Cet ID n'existe pas encore."
        # Convertion en Json String
        json_content = json.dumps(content)
        return f"{json_content}"
    except UnboundLocalError:
        return "Veuillez saisir un ID inférieur, celui-ci n'existe pas encore."


@app.route("/status/<int:doc_id>")
def show_status(doc_id):
    """Renvoie l'état de traitement du document : son statut
    Return :
        Le statut du document selon l'ID saisi par l'utilisateur
        "Pending" : par défaut s'il n'y a pas encore eu de traitement
                    et si le document n'existe pas encore
        "Failure" : document sans titre ou sans sujet ou sans contenu
        "Success" : document avec titre, sujet et du contenu texte
    """
    try:
        info = extract_data(doc_id)
        obj_python = json.loads(info)
        if obj_python["status"] == "":
            return "Statut du document : Pending"
        status = obj_python["status"]
        return f"Statut du document : {status}"
    except UnboundLocalError:
        return "Statut du document : Pending"


# A utiliser uniquement si besoin de créer un fichier .txt
@app.route("/downloads/<int:post_id>")
def download_file(post_id):
    """
    Affiche le contenu texte selon l'ID saisir par l'utilisateur.
    Le contenu texte est récupéré depuis la base de données.
    Le contenu est stocké dans un fichier ID.txt
    Le contenu de ce fichier est lu et affiché.
    Return :
        Le contenu du fichier texte selon l'ID saisi.
    """
    try:
        info = extract_data(post_id)
        obj_python = json.loads(info)
        if obj_python["status"] == "":
            return "Cet ID n'existe pas encore."
        create_text_file(post_id)
        with open(
            UPLOAD_FOLDER + "/" + str(post_id) + ".txt", "r", encoding="utf-8"
        ) as fichier:
            return fichier.read()
    except UnboundLocalError:
        return "Veuillez saisir un ID inférieur, celui-ci n'existe pas encore."


@app.route("/metadata/<int:doc_id>")
def show_metadata_id(doc_id):
    """Recupère les métadonnées du fichier (ID saisi) depuis la BDD
    Return :
        Objet contenant les données extraites du PDF (métadonnées)
    """
    try:
        info = extract_data(doc_id)
        obj_python = json.loads(info)
        if obj_python["title"] == "":
            return "Cet ID n'existe pas encore."
        # return obj_python
        return info
    except UnboundLocalError:
        return "Ce document n'existe pas encore. Saisir un ID inférieur."


@app.route("/metadata")
def show_metadata():
    """Recupère les métadonnées du fichier courant de la BDD
    Return :
        Objet contenant les données extraites du PDF (métadonnées)
    """
    info = extract_data(ID)
    obj_python = json.loads(info)
    if obj_python["title"] == "":
        return "Télécharge d'abord un fichier PDF ou saisi un ID"
    return info


@app.route("/title/<int:doc_id>")
def show_title_id(doc_id):
    """Recupère le titre du PDF correspondant à l'ID saisi depuis
    la BDD et l'affiche
    Return :
        Le titre du PDF depuis l'objet python contenant les métadonnées.
    """
    try:
        info = extract_data(doc_id)
        obj_python = json.loads(info)
        # Traitement des documents sans titre
        if obj_python["title"] == "None":
            return "Ce document n'a pas de titre."
        if obj_python["title"] == "":
            return "Cet ID n'existe pas encore."
        return obj_python["title"]
    except UnboundLocalError:
        return "Ce document n'existe pas encore. Saisir un ID inférieur."


@app.route("/title")
def show_title():
    """Recupère le titre du PDF courant depuis la BDD et l'affiche
    Return :
        Le titre du PDF depuis l'objet python contenant les métadonnées.
    """
    info = extract_data(ID)
    obj_python = json.loads(info)
    if obj_python["title"] == "None":
        return "Ce document n'a pas de titre"
    if obj_python["title"] == "":
        return "Télécharge d'abord un fichier PDF ou saisi un ID"
    return obj_python["title"]


# Les fonctions suivantes permettent (sans passer par une BDD):
# la récupération du contenu du PDF courant (métadonnées et texte)


@app.route("/titre")
def aff_titre():
    """Affiche le titre depuis les métadonnées du PDF courant,
    sans passer par une base de données.
    Return :
        Titre du PDF téléchargé par l'utilisateur
    """
    information = extract_information(PATH)
    titre = information.title
    if titre is None:
        titre = "Ce document n'a pas de titre"
    return titre


@app.route("/metadonnees")
def metadonnees():
    """Affiche les métadonnées du PDF courant sans passer par une BDD
    Return :
        Métadonnées du PDF téléchargé par l'utilisateur
    """
    information = extract_information(PATH)
    return information


@app.route("/texte")
def aff_texte():
    """Affiche le contenu texte du PDF courant sans passer par une BDD
    Récupère le texte non parsé et l'enregistre dans un fichier .txt
    Return :
        Texte du PDF téléchargé par l'utilisateur
    """
    texte = data_from_pdf(PATH)
    # Stockage le contenu du pdf dans un fichier texte ID.txt
    with open(
        UPLOAD_FOLDER + "/" + str(ID) + ".txt", "w", encoding="utf-8"
    ) as fichier:
        fichier.write(texte)
    return texte
