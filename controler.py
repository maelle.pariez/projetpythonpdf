#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------

""" PDF Storage & Extraction """


# Dépendances
try:
    from pdfminer.high_level import extract_text
    from PyPDF2 import PdfReader
    print("controler.py : Tous les modules sont bien téléchargés.")
except ModuleNotFoundError():
    print("controler.py : Modules manquants. Veuillez les installer.")


def data_from_pdf(path):

    """Affiche le contenu texte du PDF"""

    txt = extract_text(path)

    return txt


def extract_information(path):

    """Extrait les métadonnés du PDF et stocke le contenu"""

    with open(path, "rb") as my_file:

        pdf = PdfReader(my_file)

        information = pdf.metadata

    return information
