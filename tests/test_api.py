# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------

""" PDF Storage & Extraction : test de l'API """

import unittest
from flask import Flask
from ..api import create_app


class TestAPI(unittest.TestCase):
    """ Test API """
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()

    def test_create_app(self):
        """Test creation of the Flask app"""
        self.assertIsNotNone(self.app)
        self.assertIsInstance(self.app, Flask)


if __name__ == "__main__":
    unittest.main()
