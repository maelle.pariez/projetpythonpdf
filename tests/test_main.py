# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------

""" PDF Storage & Extraction : test de main """

import unittest

if __name__ == "__main__":
    unittest.main()
