# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------
""" PDF Storage & Extraction : test de view """

import unittest

from ..view import allowed_file, app


def with_client(f_f):
    """Definition du client"""

    def func(*args, **kwargs):
        with app.test_client() as client:
            return f_f(*args, client, **kwargs)

    return func


class TestFunction(unittest.TestCase):
    """Test des fonctions"""

    def test_allowed_file(self):
        """Test de différents types de fichiers."""
        self.assertTrue(allowed_file("file.pdf"))
        self.assertFalse(allowed_file("file.txt"))

    @with_client
    def test_item_invalid(self, client):
        """Test Favicon"""
        response = client.get("/favicon.ico")
        self.assertEqual(response.status_code, 200)
        response = client.get("/-1")
        self.assertEqual(response.status_code, 200)

    @with_client
    def test_endpoint(self, client):
        """Test des endpoint"""
        response = client.get("/metadata")
        self.assertEqual(response.status_code, 200)
        response = client.get("/title")
        self.assertEqual(response.status_code, 200)
        response = client.get("/titre")
        self.assertEqual(response.status_code, 500)
        response = client.get("/metadonnees")
        self.assertEqual(response.status_code, 500)
        # Test d'un endpoint imaginaire
        response = client.get("/joyeuxAnniversaire")
        self.assertEqual(response.status_code, 200)
