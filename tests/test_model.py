# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Maëlle PARIEZ
# Created Date: 15/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------

""" PDF Storage & Extraction : test de model.py """

import os
import json
from ..model import create_session, create_bdd, extract_data

# inject DB_PATH for testing
os.environ["DB_PATH"] = "test_db.db"


def test_create_bdd():
    """Test création BDD et du stockage du contenu du PDF"""

    # Test avec un PDF ayant un titre, un auteur et du texte
    path = 'tests/MesPDFdeTest/2211.00609.pdf'
    id_pdf = create_bdd(path)
    assert id_pdf is not None  # Vérification que l'ID du PDF est bien retourné
    # Récupération des données du PDF à partir de la base de données
    data = extract_data(id_pdf)
    data = json.loads(data)
    assert data["status"] == "Success"  # Vérification du statut "Success"

    # Test avec un PDF sans titre, sans auteur ou sans texte
    path = 'tests/MesPDFdeTest/22.pdf'
    id_pdf = create_bdd(path)
    assert id_pdf is not None  # Vérification que l'ID du PDF est bien retourné
    # Récupération des données du PDF à partir de la base de données
    data = extract_data(id_pdf)
    # Conversion de la réponse (chaîne de caractères) en objet Python
    data = json.loads(data)
    assert data["status"] == "Failure"  # Vérification du statut "Failure"


def test_extract_data():
    """ Test de la fonction extract_data
        Ce test ne peut fonctionner que si la base de données est remplie. """
    path = 'tests/MesPDFdeTest/test.pdf'
    doc_id = create_bdd(path)
    result = extract_data(doc_id)
    # Vérification du format de la réponse (doit être un objet JSON)
    assert isinstance(result, str)
    assert result.startswith("{") and result.endswith("}")
    # Conversion de la réponse en objet Python
    result_dict = json.loads(result)
    assert "status" in result_dict
    assert "author" in result_dict
    assert "creator" in result_dict
    assert "producer" in result_dict
    assert "subject" in result_dict
    assert "title" in result_dict
    assert "content" in result_dict


def test_set_up_class():
    """Test BDD"""
    dbb_test = create_session()
    dbb_test.execute("DROP table IF EXISTS pdf")
    dbb_test.execute("create table pdf (id text primary key, type text, actor text)") # noqa : E501
    for i in range(5):
        dbb_test.execute(
            "insert OR ignore into pdf values (:id,:type, :actor)",
            {"id": 1 + i, "type": f"type{i}", "actor": f"author{i}"},
        )
    dbb_test.commit()
